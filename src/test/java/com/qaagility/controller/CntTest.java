package java.com.qaagility.controller;

import static org.junit.Assert.assertEquals;
import com.qaagility.controller.Cnt;
import org.junit.Test;

public class CntTest {

    @Test
    public void runAddIfScenario(){
        Cnt cnt = new Cnt();
        assertEquals("Result", 9,  cnt.d(1,0));
    }

    @Test
    public void runAddElseScenario(){
        Cnt cnt = new Cnt();
        assertEquals("Result", 9,  cnt.d(3,1));
    }

}
